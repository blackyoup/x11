# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: QtWebSockets module"
DESCRIPTION="
WebSocket is a web-based protocol designed to enable two-way communication
between a client application and a remote host. It enables the two entities
to send data back and forth if the initial handshake succeeds. WebSocket is
the solution for applications that struggle to get real-time data feeds with
less network latency and minimum data exchange.

The Qt WebSockets module provides C++ and QML interfaces that enable Qt
applications to act as a server that can process WebSocket requests, or a
client that can consume data received from the server, or both."

MYOPTIONS="examples"

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
"
qtwebsockets_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

